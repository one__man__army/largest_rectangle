# README #

Problem statement:
https://www.hackerrank.com/challenges/largest-rectangle/problem

### Solution report ###

* C++
* Uses two stacks
* Space Complexity: O(n)
* Time Complexity: O(n)

### Libraries used ###

* cmath
* cstdio
* vector
* iostream
* algorithm

### Scoring ###

* Difficulty: Hard
* Max Score: 50
* Score: 50

### How to run ###
* Copy and paste solution into HackerRank 'current bufor', the submit. 