#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

class Stack{
	int cap;
	int top;
	int *tab;

public:
	Stack(int c) :cap(c){
		tab = new int[c];
		top = -1;
	}
	//See what is on the top  (element adress)
	int& Peek(){	
		return tab[top];
	}
	// Push element on the top
	void Push(int &new_obj) {
		top++;
		tab[top] = new_obj;
	}
	// Zdejmij element z góry
	void Pop(){		
		if (!IsEmpty())		
			top--;
	}
	//Check if stack is empty  -> 1, not empty -> 0
	int IsEmpty(){ 
		if (top == (-1))
			return 1;
		else
			return 0;
	}
	//Print the whole Stack
	void PokazStos(){ 
		for (int i = 0; i <= top; i++){
			cout << "i=" << i << " :  " << tab[i] << endl;
		}
	}
};

int* wczytaj_dane(int &max_l){
	cin >> max_l;
	int *dyn = new int[max_l + 1];
	for (int i = 0; i < max_l; i++){
		cin >> dyn[i];
	}
	dyn[max_l] = 0;
	return dyn;
}


void GlownaPetla(int &max_l, int *dane){	// Algorithm
	Stack ind(max_l);			// Two stacks for keeping indexes and heights
	Stack heis(max_l);
	int init_ind = 0;			// Stack initialization with 0
	int w = 0;
	int h = 0;
	int area = 0;
	int max_area = 0;
	for (int i = 0; i <= max_l; i++){		// Main loop
		if (ind.IsEmpty()){
			ind.Push(i);
			heis.Push(dane[i]);
		}
		else{
			if (ind.IsEmpty() || dane[i] > heis.Peek()){		// Values increasing
				ind.Push(i);
				heis.Push(dane[i]);
			}

			while (!heis.IsEmpty() && dane[i] < heis.Peek()){		// Local maxima
				h = heis.Peek();
				heis.Pop();
				w = (i - ind.Peek());
				if (heis.IsEmpty() || dane[i] > heis.Peek()){
						heis.Push(dane[i]);
				}
				else{
					if (heis.IsEmpty() || dane[i] < heis.Peek()){
						ind.Pop();
					}
				}
				area = w*h;
				if (area>max_area)
					max_area = area;
			}

		}
	}
	cout << max_area << endl;
}

int main() {
	int max_l;
	int *dane = wczytaj_dane(max_l);

	GlownaPetla(max_l, dane);

	/* How to use Stack
	Stack st1(10);
	int tab[] = { 1, 2, 3, 4, 5 };
	st1.Push(tab[1]);
	st1.Push(tab[2]);
	st1.Push(tab[3]);
	st1.Push(tab[4]);
	st1.PokazStos();
	cout << "empty=" << st1.IsEmpty() << endl;
	cout << "peek, " << st1.Peek() << endl;
	cout << "I will take off: " << endl;
	int a = st1.Peek();
	cout << "Popped element:" << a << endl;
	st1.Pop();
	st1.PokazStos();
	cout << "empty=" << st1.IsEmpty() << endl;
	cout << "peek, " << st1.Peek() << endl;*/

	return 0;
}